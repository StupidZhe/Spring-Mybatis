package com.sedion.zhe.Service.Impl;

import java.util.HashMap;
import java.util.Map;

import com.sedion.zhe.Service.UserLoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sedion.zhe.bean.UserBean;
import com.sedion.zhe.dao.userDao;

@Service
public class UserLoginServiceImpl implements UserLoginService {
	public UserLoginServiceImpl() {
		// TODO Auto-generated constructor stub
		System.out.println("spring注入"+this.hashCode());
	}
	@Autowired
	private userDao userDao;

	public Map<String, Object> userLogin(String useraccount, String userpsw) {
		Map<String, Object> map = new HashMap<String, Object>();
		UserBean userBean = userDao.getUser(useraccount);
		if (null == userBean) {
			map.put("res", 59);
			return map;
		} else if (!userBean.getUserpsw().equals(userpsw)) {
			map.put("res", 60);
			return map;
		} else {

			int id = userBean.getId();
			map.put("res", 100);
			map.put("id", id);
			map.put("bean", userBean);
			return map;
		}
	}
	
	public UserBean getUserbyId(int userId) {
		UserBean user = userDao.getUserbyId(userId);
		return user;
	}

}
