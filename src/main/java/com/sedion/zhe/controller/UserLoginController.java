package com.sedion.zhe.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.sedion.zhe.Service.UserLoginService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@Scope("session")
@RequestMapping(value = "/")
public class UserLoginController {
	@Autowired
	UserLoginService userLoginService;
	
	@RequestMapping(value = {"/", "/login"})
	public String userLogin(HttpServletRequest request) {
		//request.setAttribute("name", "Mr.J");
		return "userlogin";
	}
	
	@RequestMapping(value = "/logining", method=RequestMethod.POST)
	public @ResponseBody Map<String, Object> userLoginValidate(
			@RequestParam(value = "useraccount")String useraccount,
			@RequestParam(value = "userpsw") String userpsw, HttpSession session) {
		Map<String, Object> json = userLoginService.userLogin(useraccount, userpsw);
		if (null != json.get("id")) {
			session.setAttribute("userId", json.get("id"));			
		}
			return json;
	}
}
