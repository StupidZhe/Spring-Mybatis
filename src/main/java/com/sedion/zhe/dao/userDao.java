package com.sedion.zhe.dao;

import org.apache.ibatis.annotations.Param;

import com.sedion.zhe.bean.UserBean;

public interface userDao {
	 UserBean getUser(String useraccount);
	 UserBean getUserbyId(int userId);
	 void uploadHead(@Param(value = "userhead") String userhead, @Param(value = "userId") Integer userId);
	 int insertUser(UserBean userBean);
}
